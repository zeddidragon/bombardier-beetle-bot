var gulp = require('gulp');
var requireDir = require('require-dir');
require('coffee-script/register');


global.gulp = gulp;
global.task = gulp.task.bind(gulp);


require('./gulp/browserify');
requireDir('./gulp/tasks');
