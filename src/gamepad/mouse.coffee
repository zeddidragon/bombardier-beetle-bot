buttonMappings =
  0: ['button', 'rb', 1]
  2: ['button', 'lb', 1]

wheelMappings =
  '-1': ['button', 'dpadLeft', 1]
  1: ['button', 'dpadRight', 1]


mousedown = (e)->
  return unless mapping = buttonMappings[e.button]
  @gamepad[mapping[0]][mapping[1]].set "mouse#{e.button}", mapping[2]

mouseup = (e)->
  return unless mapping = buttonMappings[e.button]
  @gamepad[mapping[0]][mapping[1]].set "mouse#{e.button}", 0

wheel = (e)->
  key = sign e.deltaY
  return unless mapping = wheelMappings[key]
  @gamepad[mapping[0]][mapping[1]].set "mouseWheel#{key}", mapping[2]
  @gamepad[mapping[0]][mapping[1]].set "mouseWheel#{key}", 0

module.exports =
  class Mouse
    constructor: (@gamepad)->
      addEventListener 'mousedown', mousedown.bind this
      addEventListener 'mouseup', mouseup.bind this
      addEventListener 'wheel', _.throttle wheel.bind(this), 100
