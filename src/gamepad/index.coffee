Button = require './button'
Axis = require './axis'
mappings = require './button-mappings'
Keyboard = require './keyboard'
Mouse = require './mouse'

DEADZONE = 0.15
BUTTON_DEADZONE = 0.6


module.exports =
  class Gamepad
    constructor: ->
      @moveVec = new Vector3
      @lookVec = new Vector3
      @move =
        x: new Axis @moveVec, 'x', -1
        y: new Axis @moveVec, 'y'
      @look =
        x: new Axis @lookVec, 'x', -1
        y: new Axis @lookVec, 'y'
      @button =
        a: new Button
        b: new Button
        x: new Button
        y: new Button
        lb: new Button
        rb: new Button
        lt: new Button
        rt: new Button
        ls: new Button
        rs: new Button
        back: new Button
        start: new Button
        dpadUp: new Button
        dpadDown: new Button
        dpadLeft: new Button
        dpadRight: new Button
        home: new Button

      new Keyboard this
      new Mouse this

    update: ->
      _.invoke @button, 'clear'
      pad = navigator.getGamepads?()[0]
      return unless pad
      _.each mappings.axes, (axis, i)=>
        value = if DEADZONE < Math.abs pad.axes[i] then pad.axes[i] - DEADZONE else 0
        _.get(this, axis[0]).set "gamepapAxes#{i}", value * axis[1]
      @moveVec.x *= Math.abs(pad.axes[0]) or 1
      @moveVec.y *= Math.abs(pad.axes[1]) or 1
      @lookVec.x *= Math.abs(pad.axes[2]) or 1
      @lookVec.y *= Math.abs(pad.axes[3]) or 1
      _.each mappings.buttons, (button, i)=>
        button = @button[button]
        value = Math.abs pad.buttons[i].value
        value = if BUTTON_DEADZONE < value then value else 0
        button.set "gamepad#{i}", value
