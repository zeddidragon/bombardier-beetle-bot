module.exports =
  class Axis
    constructor: (@vector, @axis, @scalar=1)->
      @state = 0
      @bindings = {}

    set: (binding, value)->
      @bindings[binding] = value
      @state = _.sum @bindings
      @vector[@axis] = @state * @scalar
      @vector.normalize()
