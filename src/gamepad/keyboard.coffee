bindings =
  z:   ['button', 'a', true]
  x:   ['button', 'rb', true]
  c:   ['button', 'lb', true]
  r:   ['button', 'x', true]
  ' ': ['button', 'a', true]

  1: ['button', 'dpadLeft', 1]
  2: ['button', 'dpadRight', 1]
  n: ['button', 'dpadLeft', 1]
  m: ['button', 'dpadRight', 1]

  up:    ['move', 'y',  1]
  down:  ['move', 'y', -1]
  left:  ['look', 'x', -1]
  right: ['look', 'x',  1]

  w: ['move', 'y',  1]
  s: ['move', 'y', -1]
  a: ['move', 'x', -1]
  d: ['move', 'x',  1]

  i: ['look', 'y', -1]
  k: ['look', 'y',  1]
  j: ['look', 'x', -1]
  l: ['look', 'x',  1]


binding = (e)->
  _.pick bindings, [
    e.keyIdentifier?.toLowerCase(),
    String.fromCharCode(e.keyCode or e.which).toLowerCase()]

keydown = (e)->
  group = binding e
  _.each group, (val, key)=>
    @gamepad[val[0]][val[1]].set key, val[2]

keyup = (e)->
  group = binding e
  _.each group, (val, key)=>
    @gamepad[val[0]][val[1]].set key, 0


module.exports =
  class Keyboard
    constructor: (@gamepad)->
      addEventListener 'keydown', keydown.bind this
      addEventListener 'keyup', keyup.bind this
