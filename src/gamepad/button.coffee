module.exports =
  class Button
    constructor: ->
      @state = false
      @wasPressed = false
      @wasReleased = false
      @bindings = {}

    set: (binding, value)->
      @wasPressed = true if value and not @bindings[binding]
      old = @bindings[binding]
      @bindings[binding] = value
      @state = _.any @bindings
      @wasReleased = true if old and not @bindings[binding]

    clear: ->
      @wasPressed = false
      @wasReleased = false
