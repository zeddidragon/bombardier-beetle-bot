buttons:
  A: 0
  B: 1
  X: 2
  Y: 3
  L1: 4
  R1: 5
  L2: 6
  R2: 7
  SELECT: 8
  START: 9
  L3: 10
  R3: 11
  DPAD_UP: 12
  DPAD_DOWN: 13
  DPAD_LEFT: 14
  DPAD_RIGHT: 15
  HOME: 16


module.exports =
  axes:
    0: ['move.x', 1]
    1: ['move.y', -1]
    2: ['look.x', 1]
    3: ['look.y', 1]
  buttons: [
    'a'
    'b'
    'x'
    'y'
    'lb'
    'rb'
    'lt'
    'rt'
    'back'
    'start'
    'ls'
    'rs'
    'dpadUp'
    'dpadDown'
    'dpadLeft'
    'dpadRight'
    'home'
  ]
