OBJLoader = require 'three-loaders-obj'
{ImageUtils} = THREE

tmpMat = new Matrix4
tmpEul = new THREE.Euler

makeName = (filename)->
  _.camelCase(filename.split('.')[0])
url = (parts, filename)->
  _.map(parts, _.kebabCase).join('/') + "/#{filename}"

SCALE = meters 0.04
SCALE_MAT = new Matrix4
SCALE_MAT.makeScale SCALE, SCALE, SCALE
loadModel = (path)->
  modelLoader = new OBJLoader
  new Promise (resolve)->
    modelLoader.load path, resolve

loaders =
  images: (path)->
    new Promise (resolve)->
      resolve ImageUtils.loadTexture path
  models: (path)->
    loadModel path
      .then (data)->
        ret = data.children[0].geometry
        ret.applyMatrix SCALE_MAT
        ret.computeBoundingSphere()
        ret

module.exports = (assets)->
  ret = {}
  promises = []
  _.each assets, (paths, assetType)->
    sub = ret[assetType] = {}
    loader = loaders[assetType]
    _.each paths, (path)->
      name = makeName path
      promise =  loader url [assetType], path
        .then (asset)-> sub[name] = asset
      promises.push promise

  Promise.all promises
    .then -> ret
