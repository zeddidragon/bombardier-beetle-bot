{BoxGeometry, PlaneBufferGeometry, MeshLambertMaterial} = THREE

block = require './block'
needle = require './needle'
complex = require './complex'
material = require './material'

planner = require './planner'

BLOCK_SIZE = meters 64


module.exports =
  class City
    @BLOCK_SIZE: BLOCK_SIZE

    constructor: (@width, @height)->
      @ground = new PlaneBufferGeometry \
        @width * BLOCK_SIZE, @height * BLOCK_SIZE, @width, @height
      @ground.applyMatrix new Matrix4().makeRotationX -Math.PI * 0.5
      @ground.computeFaceNormals()
      @blockGeometry = block BLOCK_SIZE
      @needleGeometry = needle BLOCK_SIZE

      @material = new MeshLambertMaterial
        color: 0x343434

      @plan = planner @width, @height
      complexes = {}

      offset = new Vector3 (@width - 1) * 0.5, 0, (@height - 1) * 0.5
      @mesh = new Mesh @ground, @material
      @mesh.name = "City"
      _().range(@height).each (z)=>
        _().range(@width).each (x)=>
          switch
            when @plan[z][x] is 'N'
              building = new Mesh @needleGeometry, material 0xBBBBBB
              building.name = "NeedleBuilding"
              building.position.set x, 0, z
            when complexId = @plan[z][x]
              return if complexes[complexId]
              geo = complex @plan, {x, z}, BLOCK_SIZE
              building = new Mesh geo, material()
              building.name = "ComplexBuilding"
              complexes[complexId] = true
              window.building = building
            else
              building = new Mesh @blockGeometry, material()
              building.name = "BlockBuilding"
              building.position.set x, -Math.random(), z
          if building
            building.position
              .sub offset
              .multiplyScalar BLOCK_SIZE
            @mesh.add building
        .value()
      .value()
