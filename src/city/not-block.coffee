{BufferGeometry, BufferAttribute} = THREE

###
# 0.5
#
#        6 ----------- 7
#       /|            /|
#      / |           / |
#     4 ----------- 5  |
#     |  |          |  |
#     |  |          |  |
#     |  2 ---------|- 3
#     | /           | /
#     |/            |/
#     0 ----------- 1
#
# -0.5                0.5
###

###
module.exports = ->
  ret = new BufferGeometry

  vertices = new Float32Array 3 * 8
  for i in [0...8]
    vertices[i*3] = (i % 2) - 0.5
    vertices[i*3 + 1] = (i > 3) - 0.5
    vertices[i*3 + 2] = (i % 4 > 1) - 0.5

  ret.addAttribute 'position', new BufferAttribute vertices
###
