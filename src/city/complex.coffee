{Geometry, BoxGeometry} = THREE
VectorMap = require '../utils/vector-map'
cleanFaces = require './clean-faces'
alignUvs = require './align-uvs'
smooth = require './smooth'


tmpMat = new Matrix4
tmpVec = new Vector3
checks = [
  new Vector3
  new Vector3  1
  new Vector3  0, 0, 1
  new Vector3  1, 0, 1
]

west = new Vector3 -1
southWest = new Vector3 -1, 0, 1

tweens = _.map checks, (check)->
  ret = new Vector3
  ret
    .copy check
    .multiplyScalar 0.5

southWestTween = new Vector3 -0.5, 0, 0.5


box = ->
  new BoxGeometry 0.5, 0.5, 0.5

addBlock = (geometry, point, offset)->
  tmpVec.addVectors point, offset
  tmpMat.makeTranslation tmpVec.x, tmpVec.y, tmpVec.z
  appendage = box()
  appendage.applyMatrix tmpMat
  geometry.merge appendage
  appendage.dispose()

module.exports = (plan, start, blockSize)->
  map = new VectorMap
  id = plan[start.z][start.x]
  return unless id

  ret = new Geometry

  _.each plan, (row, j)->
    _.each row, (b, i)->
      map.set new Vector3 i, 0, j if b is id

  _.each map.map, (point)->
    _.each checks, (check, i)->
      addBlock ret, point, tweens[i] if map.check point, check
    if map.check(point, southWest) and not map.check(point, west)
      addBlock ret, point, southWestTween

  height = Math.random() * 1.5 + 0.5

  ret.mergeVertices()
  ret.applyMatrix tmpMat.makeTranslation 0, 0.24, 0
  ret.applyMatrix tmpMat.makeScale blockSize, blockSize, blockSize
  ret.applyMatrix tmpMat.makeScale 1, height, 1

  _.each ret.faceVertexUvs[0], (uvs)->
    _.invoke uvs, 'multiplyScalar', height * 0.25
  smooth ret
  cleanFaces ret
  alignUvs ret
