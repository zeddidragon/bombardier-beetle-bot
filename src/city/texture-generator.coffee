{Texture, WebGLRenderer} = THREE

renderer = new WebGLRenderer
MAX_ANISOTROPY = renderer.getMaxAnisotropy()


module.exports = ->
  canvas = document.createElement 'canvas'
  canvas.width = 64
  canvas.height = 64

  context = canvas.getContext '2d'
  context.fillStyle = '#ffffff'
  context.fillRect 0, 0, canvas.width, canvas.height

  imageData = context.getImageData 0, 0, canvas.width, canvas.height
  data = imageData.data

  for j in [1..canvas.height * 0.5]
    for i in [0...canvas.width * 0.5]
      value = _.random 64
      pos = i * 8 + j * canvas.width * 8
      _.times 3, (n)->
        data[pos + n] = data[pos + n + 4] = value

  context.putImageData imageData, 0, 0

  biggerCanvas = document.createElement 'canvas'
  biggerCanvas.width = canvas.width * 16
  biggerCanvas.height = canvas.height * 16
  context = biggerCanvas.getContext '2d'
  context.imageSmoothingEnabled = false
  context.drawImage canvas, 0, 0, biggerCanvas.width, biggerCanvas.height

  texture = new Texture biggerCanvas
  texture.anisotropy = MAX_ANISOTROPY
  texture.needsUpdate = true
  texture
