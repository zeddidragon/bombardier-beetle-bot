{MeshLambertMaterial, LinearFilter} = THREE

Color = require '../utils/color'
textureGenerator = require './texture-generator'

texture = textureGenerator()
texture.minFilter = LinearFilter
texture.magFilter = LinearFilter


module.exports = (color)->
  material = new MeshLambertMaterial
    map: texture
    color: color or Color.random()
  material
