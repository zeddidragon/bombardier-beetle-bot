# complexity: percentage of special buildings


blit = (arr, width, height, value)->
  x = _.random width - 1
  y = _.random height - 1
  width = _.random 1, 4
  height = _.random 1, 4
  _.times height, (j)->
    _.times width, (i)->
      arr[y + j]?[x + i] = value

module.exports = (width, height, complexity=0.24)->
  plan = []
  _.times height, (j)->
    plan[j] = new Array width
  n = Math.floor width * height * complexity
  _.times n, (value)->
    blit plan, width, height, value + 1
  x = _.random width
  y = _.random height
  plan[y]?[x] = 'N'
  plan
