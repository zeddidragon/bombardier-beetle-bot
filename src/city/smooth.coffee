points = 'abc'.split ''


module.exports = (geo)->
  return geo unless ABSTRACT_MODE
  connections = []
  _.each geo.faces, (face)->
    _.each points, (point)->
      i = face[point]
      connections[i] = _.union connections[i], [face.a, face.b, face.c]

  _.each geo.vertices, (vertex, i)->
    total = new Vector3
    return unless connections[i]
    _ connections[i]
      .each (n)->
        total.x += geo.vertices[n].x
        total.z += geo.vertices[n].z
      .value()
    total.x /= connections[i].length
    total.y = geo.vertices[i].y
    total.z /= connections[i].length
    connections[i] = total

  _.each connections, (newVertex, i)->
    geo.vertices[i] = newVertex if newVertex

  geo.computeFaceNormals()
  geo
