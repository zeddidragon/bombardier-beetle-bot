#Set vertexUVs on faces that point mostly up to 0

TOLERANCE = 0.1


module.exports = (geo)->
 _.each geo.faces, (face, i)->
   if TOLERANCE > face.normal.angleTo UP
     _.invoke geo.faceVertexUvs[0][i], 'set', 0, 0
 geo
