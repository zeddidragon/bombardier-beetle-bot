{BoxGeometry} = THREE
cleanFaces = require './clean-faces'
alignUvs = require './align-uvs'
smooth = require './smooth'

mat = new Matrix4


module.exports = (blockSize)->
  width = blockSize * (0.5 + ABSTRACT_MODE)
  height = blockSize * 2
  mat.makeTranslation 0, height * 0.5, 0
  ret = new BoxGeometry width, height, width
  smooth ret
  cleanFaces ret
  alignUvs ret
  ret.applyMatrix mat
  ret
