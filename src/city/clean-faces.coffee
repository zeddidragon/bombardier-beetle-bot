# Check each pair of faces against every other pair of faces
# Remove pairs that use the same four points


module.exports = (geo)->
  faces = _ geo.faces
    .map (face)->
      [face.a, face.b, face.c]
    .chunk 2
    .map (pair)->
      _.union pair[0], pair[1]
    .invoke 'sort'
    .value()
  _.each faces, (face1, j)->
    _.each faces, (face2, i)->
      if i isnt j and _.isEqual face1, face2
        delete geo.faces[i * 2]
        delete geo.faces[i * 2 + 1]
        delete geo.faces[j * 2]
        delete geo.faces[j * 2 + 1]
  _.each geo.faces, (face, i)->
    delete geo.faceVertexUvs[0][i] unless face
  geo.faceVertexUvs[0] = _.filter geo.faceVertexUvs[0]
  geo.faces = _.filter geo.faces, (face)->
    face and 0.1 < face.normal.angleTo DOWN
  geo
