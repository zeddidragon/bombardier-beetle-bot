{CylinderGeometry} = THREE

mat = new Matrix4
shrink = new Vector3 0.5, 1, 0.5
unit = 10 / 128


module.exports = (blockSize)->
  radius = blockSize * 0.4
  height = blockSize * 3

  baseHeight  = height * 0.05
  base = new CylinderGeometry \
    radius, radius, baseHeight, 32
  mat.makeTranslation 0, baseHeight * 0.49, 0
  base.applyMatrix mat

  spire = new CylinderGeometry \
    radius * 0.4, radius * 0.8, height, 32, 1, true
  mat.makeTranslation 0, height * 0.49, 0
  base.merge spire, mat

  _.each base.faceVertexUvs[0], (face)->
    _.each face, (uv)->
      uv.set 0, 0

  livingSpace = new CylinderGeometry \
    blockSize, blockSize, baseHeight * 2, 32, 3
  uvs = livingSpace.faceVertexUvs[0]
  _ uvs
    .drop 64 * 3
    .each (face)->
      _.each face, (uv)->
        uv.set 0, 0
    .value()
  _ uvs
    .take 64 * 3
    .chunk 6
    .each (chunk)->
      chunk[2][0].y = unit
      chunk[2][1].y = 0
      chunk[2][2].y = unit
      chunk[3][0].y = 0
      chunk[3][1].y = 0
      chunk[3][2].y = unit
    .value()

  expansion = meters 10
  verts = livingSpace.vertices
  _ verts
    .take 33
    .each (v)-> v.multiply shrink
    .value()
  _ verts
    .takeRight 35
    .each (v)-> v.multiply shrink
    .value()
  _ verts
    .take 66
    .each (v)-> v.y += expansion
    .value()
  verts[verts.length - 2].y += expansion
  livingSpace.computeFaceNormals()
  livingSpace.computeVertexNormals()
  mat.makeTranslation 0, height, 0
  base.merge livingSpace, mat

  base
