htmlToVdom = require 'html-to-vdom'
vDom = require 'virtual-dom'

template = require './templates/hud'

virtualize = htmlToVdom vDom


module.exports =
  class Hud
    constructor: (game)->
      @vTree = @render game
      @element = vDom.create @vTree
      document.body.appendChild @element

    render: (world)->
      virtualize template world

    update: (world)->
      newTree = @render world
      @element = vDom.patch @element, vDom.diff @vTree, newTree
      @vTree = newTree
