Timer = require './timer'
Hud = require './hud'
Gamepad = require './gamepad'
World = require './world'
City = require './city'
Player = require './entity/player'
EnemyDirector = require './ai/enemy-director'
Indicator = require './map/indicator'
PlayerIndicator = require './map/player-indicator'
Pathfinder = require './map/pathfinder'
explosions = require './entity/fx/explosion'
aiMap = require './map/ai-map'
aStarWorkers = require './ai/a-star-workers'

module.exports = (data)->
  city = new City 24, 24
  data.aiMap = aiMap city
  data.targets =
    player: new Object3D
    playerHoming: new Object3D
    enemy: new Object3D
    enemyHoming: new Object3D
  world = new World data
  world.add city
  pathfinder = new Pathfinder data.aiMap
  ret =
    data: data
    world: world
    city: city
    timer: new Timer
    player: world.add new Player world, data
    enemies: new EnemyDirector world, data, pathfinder
    gamepad: new Gamepad
    aiMap: data.aiMap
    pathfinder: pathfinder
  world.map.add new PlayerIndicator ret.player

  pathfinder.toCityCoords pathfinder.nearest pathfinder.toMapCoords ret.player.position

  explosions.init world
  aStarWorkers.init data.aiMap

  data.targets.player.children.push ret.enemies.collisions
  data.targets.player.children.push city.mesh
  data.targets.playerHoming.children.push ret.enemies.collisions
  data.targets.enemy.children.push ret.player.avatar
  data.targets.enemyHoming.children.push ret.player.avatar

  ret.coords = 'xyz'
  ret.hud = new Hud ret
  ret
