raf = require 'raf'
Arrow = require './entity/modules/arrow'
explosion = require './entity/fx/explosion'


update = (@game)->
  tick()

tick = ->
  @game.timer.update()
  @game.player.update @game
  @game.world.map.update @game
  @game.world.render()
  @game.hud.update @game
  @game.gamepad.update()
  @game.enemies.update @game
  Arrow.update @game
  explosion.update @game
  raf tick

module.exports = update
