now = require 'performance-now'


module.exports =
  class Timer
    constructor: ->
      @index = 0
      @deltas = new Float32Array(10)
      @lastTime = now()
      @fps = 0

    update: ->
      time = now()
      @delta = time - @lastTime
      @lastTime = time
      @deltas[@index++] = @delta
      if @index >= @deltas.length
        @fps = 1000 * @deltas.length / _.sum(@deltas)
        @index = 0
      # Ensure that lagspikes don't make objects fly ridiculous distances.
      @delta = Math.min(@delta, 100) / 1000
