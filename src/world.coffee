{Scene, DirectionalLight, PerspectiveCamera, WebGLRenderer} = THREE

Skybox = require './skybox'
Color = require './utils/color'
Minimap = require './map/minimap'


module.exports =
  class World
    constructor: (data)->
      @scene = new Scene()
      @renderer = new WebGLRenderer()
      @renderer.autoClearColor = false

      @camera = new PerspectiveCamera 35, 0.5, 0.1, meters(3000)
      @camera.position.set meters(12), meters(36), meters(24)
      @camera.lookAt @scene.position
      @add @camera


      color = Color.random 0.5
      @skybox = new Skybox data.images.skyboxMountain, @camera, color
      @map = new Minimap data.aiMap, @camera

      @light = new DirectionalLight color, 0.5
      @light.position.set 48, 24, -36
      @add @light

      @backlight = new DirectionalLight color, 0.2
      @backlight.position.set -36, 48, 24
      @add @backlight

      @resize()
      addEventListener 'resize', @resize.bind @
      document.body.appendChild @renderer.domElement

    add: (obj)->
      obj.added = true
      @scene.add obj.mesh or obj
      obj

    remove: (obj)->
      obj.added = false
      @scene.remove obj.mesh or obj
      obj

    render: ->
      @skybox.camera.quaternion.copy @camera.quaternion
      @renderer.render @skybox.scene, @skybox.camera
      @renderer.render @scene, @camera
      @renderer.render @map.scene, @map.camera
      this

    resize: ->
      @renderer.setSize innerWidth, innerHeight
      @camera.aspect = innerWidth / innerHeight
      @camera.updateProjectionMatrix()
      @skybox.resize innerWidth, innerHeight
      @map.resize innerWidth, innerHeight
      this
