@Promise = require 'bluebird'

@THREE = require 'three'

@_ = require 'lodash'
@moment = require 'moment'

{@Vector3, @Euler, @Quaternion, @Matrix4, @Mesh, @Object3D} = @THREE

@METER = 1.2
@KILOMETER = @METER * 1000
@meters = (x)-> x * @METER
@km = (x)-> x * @METER * 1000
@kmh = (x)-> x * @METER / 3.6
@WORLD_SIZE = @meters 64 * 15

@GRAVITY = new @Vector3 0, @meters(-30), 0
@UP = new @Vector3 0, 1, 0
@DOWN = new @Vector3 0, -1, 0
@NORTH = new @Vector3 0, 0, 1
@EAST = new @Vector3 1, 0, 0
@NULL = new @Vector3

@ABSTRACT_MODE = false

@puts = console.log.bind console
@format = (n, i)->
  return 0 unless n
  prec = Math.floor(Math.log10(Math.abs(n)) + 1 + i) or 2
  return 0 if prec < 1
  n.toPrecision prec

DOZ_DIGITS = '0123456789XE'

@toDoz = (n)->
  output = []
  negative = n < 0
  n = Math.floor Math.abs n

  while n
    output.push DOZ_DIGITS[n % DOZ_DIGITS.length]
    n = Math.floor n / DOZ_DIGITS.length

  output = output.reverse().join('') + if negative then '-' else ''
  output or 0

@fromDoz = (str)->
  ret = 0

  str
    .split ''
    .reverse()
    .forEach (digit, i)->
      ret += DOZ_DIGITS.indexOf(digit) * Math.pow DOZ_DIGITS.length, i

  ret

@sign = (x)->
  if x < 0 then -1 else 1
@clamp = (x, min, max)->
  Math.max min, Math.min max, x

@_.extend window, this
