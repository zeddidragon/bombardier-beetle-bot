{Raycaster} = THREE

caster = new Raycaster
ray = caster.ray


module.exports =
  class SphereHitbox
    constructor: (radius, height, options)->
      @radius = radius

    touch: (other, position, vector, speed=0)->
      caster.far = @radius * 2 + speed
      ray.origin
        .copy vector
        .negate()
        .setLength @radius
      ray.direction
        .copy vector
        .normalize()
      ray.origin.add position
      intersections = caster.intersectObject other.mesh, true
      intersections[0]
