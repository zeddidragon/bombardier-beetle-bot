worker = new Worker URL.createObjectURL require './a-star'


callbacks = []
counter = 0

freeId = ->
  ret = callbacks.indexOf undefined
  if ret < 0 then callbacks.length else ret

worker.addEventListener 'message', ({data})->
  callbacks[data.$id].onAstar data.path

worker.addEventListener 'error', ({message})->
  console.error message


module.exports =
  init: (grid)->
    worker.postMessage {grid}

  path: (start, end, instance, opts)->
    $id = freeId()
    callbacks[$id] = instance
    worker.postMessage {$id, start, end, opts}
