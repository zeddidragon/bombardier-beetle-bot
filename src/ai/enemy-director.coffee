{Object3D} = THREE

Collection  =require '../utils/collection'
Indicator = require '../map/indicator'
Car = require '../entity/enemies/car'


module.exports =
  class EnemyDirector extends Collection
    constructor: (@world, @data, @pathfinder)->
      super
      @collisions = new Object3D
      @collisions.name = "EnemiesCollision"
      @collisions.visible = false
      @world.add @collisions
      _.times 32, @spawn.bind this, Car
      @maxEnemies = 256
      @remove = @remove.bind this

    update: (game)->
      @each (enemy)->
        enemy.update game
        enemy.state isnt 'dead'
      , @remove

      @timer += game.timer.delta
      @spawn Car, game.aiMap if @timer > @nextSpawnTime
      this

    create: (Klass)->
      new Klass @world, @data

    remove: (enemy)->
      @world.remove enemy
      @collisions.remove enemy.collisionMesh
      @world.map.remove enemy.mapIndicator
      this

    spawn: (Klass)->
      @timer = 0
      @nextSpawnTime = _.random 1, 3, true
      return if @count >= @maxEnemies
      enemy = @new @create.bind this, Klass
      enemy.reset()
      @pathfinder.random enemy.position
      @world.add enemy
      enemy.mapIndicator or= new Indicator enemy
      @world.map.add enemy.mapIndicator
      @collisions.add enemy.collisionMesh
      enemy
