{Scene, BoxGeometry, MeshBasicMaterial, BackSide} = THREE
mapFace = (faces, faceIndex, coords)->
  face = faces[0][faceIndex * 2]
  face[0].set coords.right, coords.top
  face[1].set coords.right, coords.bottom
  face[2].set coords.left, coords.top
  face = faces[0][faceIndex * 2 + 1]
  face[0].set coords.right, coords.bottom
  face[1].set coords.left, coords.bottom
  face[2].set coords.left, coords.top

mapFaceRotated = (faces, faceIndex, coords)->
  face = faces[0][faceIndex * 2]
  face[0].set coords.top, coords.right
  face[1].set coords.bottom, coords.right
  face[2].set coords.top, coords.left
  face = faces[0][faceIndex * 2 + 1]
  face[0].set coords.bottom, coords.right
  face[1].set coords.bottom, coords.left
  face[2].set coords.top, coords.left


module.exports =
  class Skybox
    constructor: (texture, camera, color)->
      @geometry = new BoxGeometry 12, 6, 12
      mapFace @geometry.faceVertexUvs, 5,
        top: 0.25
        left: 0.75
        right: 0.25
        bottom: 0
      mapFace @geometry.faceVertexUvs, 4,
        top: 0.75
        left: 0.25
        right: 0.75
        bottom: 1
      mapFaceRotated @geometry.faceVertexUvs, 1,
        top: 0.75
        left: 0.75
        right: 0.25
        bottom: 1
      mapFaceRotated @geometry.faceVertexUvs, 0,
        top: 0.25
        left: 0.25
        right: 0.75
        bottom: 0
      mapFace @geometry.faceVertexUvs, 2,
        top: 0.25
        left: 0.25
        right: 0.75
        bottom: 0.75
      @material = new MeshBasicMaterial
        map: texture
        side: BackSide
        color: color
      @mesh = new Mesh @geometry, @material
      @mesh.position.y = 2
      @scene = new Scene
      @scene.add @mesh
      @camera = camera.clone()
      @camera.position.copy NULL

    resize: (width, height)->
      @camera.aspect = width / height
      @camera.updateProjectionMatrix()
