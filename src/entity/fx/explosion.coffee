{SphereGeometry, MeshBasicMaterial} = THREE
MeshCollection = require '../../utils/collection/mesh'


geometry = new SphereGeometry 1
material = new MeshBasicMaterial
  color: 0xFFFFAA
  transparent: true
  opacity: 0.5
collection = null

grow = new Vector3

START = new Vector3 1, 1, 1
DEFAULTS =
  start: 1
  growSpeed: 4
  maxSize: meters 2

module.exports =
  init: (world)->
    collection = new MeshCollection geometry, material
    world.add collection

  update: (game)->
    collection.each (explosion)->
      explosion.scale.multiplyScalar 1 + game.timer.delta * explosion.growSpeed
      explosion.scale.x < explosion.maxSize

  create: (game, position, options)->
    explosion = collection.new()
    _.extend explosion, DEFAULTS, options
    explosion.growSpeed = explosion.maxSize * 2
    explosion.scale
      .copy NULL
      .addScalar explosion.start
    explosion.position.copy position
