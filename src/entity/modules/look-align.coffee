direction = new Vector3

init = ->
  @rotationSpeed or= 2.4

update = (game)->
  return
  max = @rotationSpeed * game.timer.delta
  angle = Math.min max, Math.abs @lookRotation.y * game.timer.delta
  angle *= sign @lookRotation.y
  direction
    .set 0, 0, 1
    .applyQuaternion @quaternion
    .applyAxisAngle @up, angle
    .add @position
  @lookAt direction
  @lookRotationVec.y -= angle

module.exports = (obj, game)->
  init.call obj
  update.bind obj
