force = new Vector3

init = ->
  @speed or= new Vector3
  @position or= new Vector3
  @jumpForce or= kmh 60

update = (game)->
  @jump game if @ground and game.gamepad.button.a.wasPressed

jump = (game)->
  force
    .copy @up
    .multiplyScalar @jumpForce
  @position.addScaled force, game.timer.delta
  @speed.add force
  @ground = undefined

module.exports = (obj)->
  init.call obj
  obj.jump = jump.bind obj
  update.bind obj
