init = ->
  @isAiming = false

update = (game)->
  buttons = game.gamepad.button
  @isAiming = buttons.lb.state
  @previousGun() if buttons.dpadLeft.wasPressed
  @nextGun() if buttons.y.wasPressed or buttons.dpadRight.wasPressed
  @gun.reload() if buttons.x.wasPressed
  @gun.fire game, this if buttons.rb.state

module.exports = (obj)->
  init.call obj
  update.bind obj

