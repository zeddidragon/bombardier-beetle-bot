init = ->
  @acc or= new Vector3
  @maxAcc or= kmh 320
  @frictionConstant or= kmh 128
  @frictionScalar or= 0.025

update = (game)->
  @acc
    .copy NULL
    .addInOrder game.gamepad.moveVec, 'xzy'
    .multiplyScalar @maxAcc
    .applyQuaternion @avatar.quaternion
    .applyQuaternion @quaternion
  @friction = @speed.lengthSq() * @frictionScalar
  if @ground then @friction += @frictionConstant else @acc.add GRAVITY


module.exports = (obj)->
  init.call obj
  update.bind obj
