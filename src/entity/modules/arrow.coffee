arrows = []
direction = new Vector3

module.exports =
  class Arrow
    constructor: (color, @scalar=1)->
      @position = new Vector3
      @vector = new Vector3 0, 0, -1
      @mesh = new THREE.ArrowHelper @position, @vector, 1, color
      arrows.push this

    update: ->
      @mesh.position.copy @position
      direction
        .copy @vector
        .normalize()
      @mesh.setDirection direction
      @mesh.setLength @vector.length() * @scalar

    copy: (v1, v2)->
      @position.copy v1
      @vector.copy v2

    set: (v1, v2)->
      @position = v1
      @vector = v2

    @init: (game)->
      _.each arrows, game.world.add.bind game.world

    @update: (game)->
      _.invoke arrows, 'update'
