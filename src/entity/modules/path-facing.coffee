init = (obj)->
  @toNode or= new Vector3
  

update = (game)->
  if @pathSpeed and not @position.equals @toNode
    @lookAt @toNode


module.exports = (obj)->
  init.call obj
  update.bind obj
