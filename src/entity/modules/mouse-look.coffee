lock = require 'pointer-lock'
fullscreen = require 'fullscreen'

module.exports = (vector)->
  return alert 'mouselock unavailable' unless lock.available()
  body = document.getElementById 'body'
  pointer = lock body
  pointer.on 'attain', (movements)->
    movements.on 'data', (move)->
      vector.x -= move.dx
      vector.y += move.dy
  pointer.on 'needs-fullscreen', ->
    fs = fullscreen body
    fs.once 'attain', ->
      pointer.request()
    fs.request()
