tmpVec = new Vector3
tmpPos = new Vector3
tmpQua = new Quaternion

forward = new Vector3
right = new Vector3

init = ->
  @up or= new Vector3
  @speed or= new Vector3
  @position or= new Vector3
  @quaternion or= new Quaternion

update = (game)->
  contact = @ground =
    collideWalls(this, game) or
    collideGround(this, game) or
    collideCorner(this, game)
  if contact
    tmpVec
      .copy contact.face.normal
      .multiplyScalar @height * 0.9
    @position.copy contact.point
    tmpVec.copy contact.face.normal
    @speed.add tmpVec.multiplyScalar -tmpVec.dot @speed
    align this, contact.face.normal
  else
    align this, UP

align = (obj, normal)->
  return if obj.up.equals normal
  tmpQua.setFromUnitVectors obj.up, normal
  obj.up.copy normal
  obj.quaternion.multiplyQuaternions tmpQua, obj.quaternion

collideGround = (obj, game)->
  return unless obj.ground or 0 > obj.speed.dot UP
  tmpPos
    .copy obj.position
    .addScaled obj.up, obj.height * 0.5
  tmpVec
    .copy if obj.ground then obj.up else UP
    .negate()
  obj.hitbox.touch game.city,
    tmpPos, tmpVec, obj.speed.length() * game.timer.delta

collideWalls = (obj, game)->
  tmpPos
    .copy obj.position
    .addScaled obj.up, obj.height * 0.5
  obj.hitbox.touch game.city,
    tmpPos, obj.speed, obj.speed.length() * game.timer.delta

collideCorner = (obj, game)->
  return unless obj.ground
  tmpVec
    .copy obj.speed
    .negate()
  tmpPos
    .copy obj.position
    .addScaled obj.up, -0.1
  obj.hitbox.touch game.city,
    tmpPos, tmpVec, tmpVec.length() * game.timer.delta

module.exports = (obj)->
  init.call obj
  update.bind obj
