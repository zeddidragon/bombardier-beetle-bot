init = ->
  @friction or= 0
  @oldFriction or= 0
  @speed or= new Vector3

update = (game)->
  @oldFriction = (@oldFriction + @friction) * 0.5 * game.timer.delta
  length = @speed.length()
  @speed.setLength Math.max 0, length - @oldFriction
  @oldFriction = @friction

module.exports = (obj)->
  init.call obj
  update.bind obj
