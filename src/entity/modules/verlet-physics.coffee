init = ->
  @acc or= new Vector3
  @oldAcc or= new Vector3
  @friction or= 0
  @oldFriction or= 0
  @speed or= new Vector3
  @position or= new Vector3

update = (game)->
  delta = game.timer.delta
  @oldAcc
    .add @acc
    .multiplyScalar 0.5 * delta
  @position
    .addScaled @oldAcc, delta
    .addScaled @speed, delta
  @speed.add @oldAcc
  @oldAcc.copy @acc

module.exports = (obj)->
  init.call obj
  update.bind obj
