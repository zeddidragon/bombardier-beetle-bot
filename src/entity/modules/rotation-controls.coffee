{mouseLook} = require '../modules'
maxLook = Math.PI * 0.475
minLook = -maxLook

lookX = new Quaternion

init = ->
  @mouse or= new Vector3
  @lookY = new Quaternion
  @lookYAngle = 0
  @mouseSensitivity or= 0.025
  @lookSpeed or= 2
  @facing = new Quaternion
  @looking = new Quaternion
  mouseLook @mouse

update = (game)->
  modifier = if @isAiming then 0.3 else 1
  lookSpeed = @lookSpeed * game.timer.delta * modifier

  lookXAngle = game.gamepad.lookVec.x + @mouse.x * @mouseSensitivity
  lookX.setFromAxisAngle UP, lookXAngle * lookSpeed
  @lookYAngle += (game.gamepad.lookVec.y + @mouse.y * @mouseSensitivity) * lookSpeed
  @lookYAngle = clamp @lookYAngle, minLook, maxLook
  @lookY.setFromAxisAngle EAST, @lookYAngle
  @avatar.quaternion.multiply lookX
  @facing.multiplyQuaternions @quaternion, @avatar.quaternion
  @looking.multiplyQuaternions @facing, @lookY
  @mouse.copy NULL

module.exports = (obj, game)->
  init.call obj
  update.bind obj
