init = ->
  @guns or= []
  @gunIndex = 0
  @gun = @guns[@gunIndex]

update = (game)->
  gun.update game, this for gun in @guns

nextGun = ->
  @gunIndex++
  @gunIndex = 0 if @gunIndex >= @guns.length
  @gun = @guns[@gunIndex]

previousGun = ->
  @gunIndex = @guns.length if @gunIndex <= 0
  @gunIndex--
  @gun = @guns[@gunIndex]


module.exports = (obj)->
  init.call obj
  obj.nextGun = nextGun.bind obj
  obj.previousGun = previousGun.bind obj
  update.bind obj
