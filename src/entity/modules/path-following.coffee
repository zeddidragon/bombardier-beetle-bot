tmpVec = new Vector3
start = new Vector3
dest = new Vector3

init = (obj)->
  @isPathing = false
  @fromNode or= new Vector3
  @toNode or=  new Vector3
  @fromNode.copy @toNode.copy @position
  @moveSpeed or= kmh 50
  @path or= []

update = (game)->
  return unless @pathSpeed
  @position.lerpVectors @fromNode, @toNode, @pathProgress
  @pathProgress += @pathSpeed * game.timer.delta
  if @pathProgress >= 1
    @nextPathNode()

pathTo = (pathfinder, position)->
  return if @$isPathing
  @isPathing = true
  @pathfinder = pathfinder
  @pathfinder.nearest pathfinder.toMapCoords dest.copy position
  @pathfinder.toMapCoords start.copy @position
  pathfinder.path start, dest, this,
    max: 16
    deadzone: 8

onAStar = (path)->
  @isPathing = false
  @pathfinder.toMapCoords start.copy @position
  @path = @pathfinder.cutToNearest start, path
  @nextPathNode()
  @nextPathNode()

nextPathNode = ->
  node = @pathfinder.next @path, @toNode
  @pathProgress = 0
  if node
    @fromNode.copy @position
    @pathSpeed =
      @moveSpeed / tmpVec
      .copy @toNode
      .sub @fromNode
      .length()
    @pathSpeed or= 1
  else
    @pathSpeed = 0

module.exports = (obj)->
  init.call obj
  obj.pathTo = pathTo.bind obj
  obj.nextPathNode = nextPathNode.bind obj
  obj.onAstar = onAStar.bind obj
  update.bind obj
