init = ->
  @position or= new Vector3
  @targetPosition or= new Vector3
  @rotation or= new Euler 0, 0, 0, 'YXZ'
  @distance or= meters 5

update: (game)->
  @position
    .set 0, 0, 1
    .applyEuler @rotation
    .multiplyScalar @distance
    .add @targetPosition


module.exports = (obj)->
  init.call obj
  update.bind obj
