Entity = require './entity'
collisionPoint = require '../utils/collision/point'

offset = new Vector3
focus = new Vector3
position = new Vector3

module.exports =
  class CameraEntity extends Entity
    constructor: (@camera, @target)->
      @tension = 6
      @distance = meters 18
      @offset = new Vector3 0, meters(4), 0
      @aimDistance = 0
      @aimOffset = new Vector3 0, meters(3), 0
      @mediumQua = new Quaternion
      @mediumQua.copy @target.quaternion
      super @camera, []

    update: (game)->
      tension = @tension * game.timer.delta
      distance = if @target.isAiming then @aimDistance else @distance
      @mediumQua.slerp @target.quaternion, tension
      offset
        .copy if @target.isAiming then @aimOffset else @offset
        .applyQuaternion @target.avatar.quaternion
        .applyQuaternion @mediumQua

      focus
        .set 0, 0, 1
        .applyQuaternion @target.lookY
        .applyQuaternion @target.avatar.quaternion
        .applyQuaternion @mediumQua

      position
        .copy focus
        .multiplyScalar -distance
        .add @target.position
        .add offset
      @up.lerp @target.up, tension
      @position.copy position
      focus
        .multiplyScalar km 3000
        .add @target.position
      @lookAt focus
