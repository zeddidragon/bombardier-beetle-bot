module.exports = (game, state)->
  @[_.camelCase "start-#{state}"]? game
  @state = state
