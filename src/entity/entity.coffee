module.exports =
  class Entity
    constructor: (@mesh, @modules=[])->
      {@position, @quaternion, @up, @scale} = @mesh
      @maxHp = @hp = 1
      @mesh.center or= new Vector3
      if geo = @mesh.geometry
        geo.computeBoundingSphere() unless geo.boundingSphere
        @mesh.center.copy geo.boundingSphere.center
      @lookAt = @mesh.lookAt.bind @mesh
      _.each @modules, (module, name)=>

        @[name] = module this
    update: _.noop

