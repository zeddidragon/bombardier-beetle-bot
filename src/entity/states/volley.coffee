module.exports =
  startVolley: (game)->
    @timer = 0
    @counter = 0

  volley: (game)->
    @timer += game.timer.delta
    if @volleyTimer > @gun.delay
      @counter++
      @timer -= @gun.delay
      @gun.position
      @gun.fire game, this
      if @counter >= @gun.max
        @decide game
