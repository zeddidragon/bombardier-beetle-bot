module.exports =
  startDying: (game)->
    @timer = 0

  dying: (game)->
    @timer += game.timer.delta
    @mesh.visible = !@mesh.visible
    @changeState game, 'dead' if @timer > 2
    @physics game
    @frictionPhysics game
