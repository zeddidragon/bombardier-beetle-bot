module.exports =
  startHunting: (game)->
    @timer = 0
    @timerMax = _.random 4, 10, true
    @pathTo game.pathfinder, game.player.position if game

  hunting: (game)->
    @pathFollowing game
    @pathFacing game
    @timer += game.timer.delta
    @decide game if @timer > @timerMax
