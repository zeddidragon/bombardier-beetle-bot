{Mesh, MeshLambertMaterial, MeshBasicMaterial} = THREE

Entity = require '../entity'
explosion = require '../fx/explosion'

basicMaterial = new MeshBasicMaterial
material = new MeshLambertMaterial
  color: 0x33AA44
modules =
  physics: require '../modules/verlet-physics'
  frictionPhysics: require '../modules/verlet-friction'
  pathFollowing: require '../modules/path-following'
  pathFacing: require '../modules/path-facing'

states = [
  require '../states/hunting'
  require '../states/dying'
  require '../states/volley'
]

changeState = require '../state-machine'
VolleyWall = require '../gun/volley-wall'
gun = null


module.exports =
  class Car extends Entity
    _.extend @prototype, states...

    constructor: (world, data)->
      @gun = gun or=
        new VolleyWall world, data, data.targets.enemy, data.targets.enemyHoming
      @mesh = new Mesh data.models.armoredCar, material
      @weight = 10
      @collisionMesh = new Mesh data.models.collisionArmoredCar, basicMaterial
      @collisionMesh.onHit = @onHit.bind this
      @collisionMesh.center = @collisionMesh.geometry.boundingSphere.center
      super @mesh, modules
      @hp = 18
      @mesh.name = "Car"
      @mesh.onHit = @onHit.bind this
      @maxHp = 18
      @timer = 3
      @timerLimit = _.random 0, 5, true
      @counter = 0
      @reset()

    changeState: changeState

    reset: ->
      @mesh.visible = true
      @changeState null, 'hunting'
      @weight = 10
      @speed.copy NULL
      @path = []
      @pathSpeed = 0
      @hp = @maxHp

    update: (game)->
      @[@state] game
      @collisionMesh.position.copy @position
      @collisionMesh.quaternion.copy @quaternion

    decide: (game)->
      @startHunting game
      ###
        random = _.random 100
        if random > 50
          @startHunting game
        else
          @startVolley game
      ###

    onHit: (game, source)->
      @hp -= source.damage
      if @state is 'dying'
        @speed.addScaled source.speed, source.weight / @weight
      else if @hp <= 0
        explosion.create game, @position,
          start: meters 20
          maxSize: meters 18
        @speed.addScaled source.speed, source.weight / @weight * 2
        @changeState game, 'dying'
