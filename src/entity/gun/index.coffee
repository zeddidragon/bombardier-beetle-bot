EntityCollection = require 'collection/entity'
Bullet = require './ammo/bullet'

module.exports =
  class Gun
    constructor: (world, data, @targets, @homingTargets)->
      @offset = new Vector3 0, meters(3), 0
      @bullets = new EntityCollection Bullet, world, data
      @bullets.name = "Bullets"
      @bullets.instanceName = "Bullet"
      @name = "Gun"
      world.add @bullets

      @delay = 500
      @spread = new Vector3
      @amplitude = 0
      @offset = new Vector3 0, meters(3), 0
      @count = 1
      @life = 2
      @speed = 144
      @last = 0
      @max = 12
      @ammo = 12
      @reloadTime = 1
      @reloadProgress = 0
      @weight = 1
      @damage = 1
      @splash = 0
      @radius = 1
      @homing = 0
      @

    fire: (game, obj)->
      now = Date.now()
      return if now - @last < @delay or not @ammo
      @ammo--
      @last = now
      for i in [1..@count] by 1
        @bullets.new()
          .ready this
          .aim obj, this
      this

    update: (game, obj)->
      @reloadProgress += game.timer.delta unless @ammo
      if @reloadProgress > @reloadTime
        @reloadProgress = 0
        @ammo = @max
      @bullets.update game
      this

    reload: ->
      @ammo = 0 unless @ammo is @max

    setRof: (rof)->
      @delay = 1000 / rof

    setAccuracy: (accuracy)->
      spread = (1 - accuracy) * (1 - accuracy)
      @spread.set spread, spread, spread

    setDistance: (distance)->
      @life = distance / @speed

    setAmmo: (ammo)->
      @max = ammo
      @ammo = ammo
