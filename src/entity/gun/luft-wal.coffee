Gun = require './index'
Rocket = require './ammo/rocket'

module.exports =
  class LuftWal extends Gun
    constructor: (world, data, targets)->
      super
      @bullets.class = Rocket
      @name = 'Luft Wal'
      @amplitude = 0.2
      @speed = kmh 128
      @damage = 20
      @splash = 100
      @radius = meters 64
      @setRof 1
      @setAccuracy 1
      @setDistance km 3
      @setAmmo 1
      @reloadTime = 10
      @homing = 1

