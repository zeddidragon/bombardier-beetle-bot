Gun = require './index'
Rocket = require './ammo/rocket'
module.exports =
  class VolleyWall extends Gun
    constructor: (world, data, targets)->
      super
      @bullets.class = Rocket
      @name = 'Volley Wall'
      @amplitude = 0.4
      @speed = kmh 800
      @damage = 26
      @splash = 12
      @radius = meters 24
      @setRof 2
      @setAccuracy 0.4
      @setDistance km 1
      @max = 6
      @ammo = -1  # Infinite ammo
      @reloadTime = 5
      @homing = 0.4

