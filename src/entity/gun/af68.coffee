Gun = require './index'

module.exports =
  class Af68 extends Gun
    constructor: (world, data, targets)->
      super
      @name = 'AF68'
      @amplitude = 0.2
      @speed = km 1.2
      @damage = 3
      @setRof 24
      @setAccuracy 0.85
      @setDistance km 0.8
      @setAmmo 144
      @reloadTime = 3
