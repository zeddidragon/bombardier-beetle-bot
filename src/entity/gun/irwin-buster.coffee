Gun = require './index'
Rocket = require './ammo/rocket'

module.exports =
  class IrwinBuster extends Gun
    constructor: (world, data, targets)->
      super
      @bullets.class = Rocket
      @name = 'Irwin Buster'
      @amplitude = 0.2
      @speed = kmh 512
      @damage = 10
      @splash = 10
      @radius = meters 36
      @setRof 1
      @setAccuracy 0.9
      @setDistance km 3
      @setAmmo 3
      @reloadTime = 7
