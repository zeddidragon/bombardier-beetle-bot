Gun = require './index'

module.exports =
  class Kangaroo extends Gun
    constructor: (world, data, targets)->
      super
      @name = "Kangaroo #{toDoz(1986)}"
      @amplitude = 0.4
      @speed = km 0.7
      @count = 36
      @setRof 2
      @spread.set 0.3 * 0.3, 0.2 * 0.2, 0
      @setDistance km 0.25
      @setAmmo 15
      @reloadTime = 6
