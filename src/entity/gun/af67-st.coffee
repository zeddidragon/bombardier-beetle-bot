Gun = require './index'

module.exports =
  class Af67St extends Gun
    constructor: (world, data, targets)->
      super
      @name = "AF67-ST"
      @speed = km 1.4
      @setRof 4
      @setAccuracy 0.99
      @setDistance km 1.2
      @setAmmo 24
      @reloadTime = 4
      @damage = 6
