Bullet = require './bullet'

module.exports =
  class Missile extends Bullet
    constructor: (world, data)->
      @mesh = new Mesh geometry, material
      super
      @splash = 1
      @radius = meters 36
      @homing = 0.1

