{LineBasicMaterial, Geometry, Line} = THREE

COPY_ATTRS = [
  'damage'
  'splash'
  'targets'
  'weight'
  'life'
  'radius'
  'homing'
  'homingTargets'
]

explosion = require 'fx/explosion'
collisionRay = require 'collision/ray'
collisionSphere = require 'collision/sphere'
all = require 'collision/all'

geometry = new Geometry
geometry.vertices.push new Vector3
geometry.vertices.push new Vector3 0, 0, -1
material = new LineBasicMaterial
  color: 0xFFFF00
  linewidth: 12

tmpVec = new Vector3

module.exports =
  class Bullet
    constructor: (world, data)->
      @mesh or= new Line geometry, material
      {@position, @quaternion} = @mesh
      @lookAt = @mesh.lookAt.bind @mesh
      @targets = null
      @homingTargets = null
      @target = null

      @damage = 1
      @weight = 1
      @speed = new Vector3
      @speedScalar = 1
      @life = 1
      @radius = 1
      @splash = 0
      @homing = 0

    update: (game)->
      delta = game.timer.delta
      @life -= delta
      return false if @life < 0
      collision = collisionRay @targets,
        origin: @position
        direction: @speed
        distance: @speedScalar * game.timer.delta

      if collision.length
        @collide game, collision[0]
        false
      else
        @home game if @homing and @target
        @position.addScaled @speed, delta

    collide: (game, collision)->
      explosion.create game, collision.point, maxSize: @radius
      collision.object.onHit? game, this
      @explode() if @splash

    ready: (options)->
      _.extend this, _.pick options, COPY_ATTRS
      @speedScalar =
        ((Math.random() - 0.5) * options.amplitude + 1) * (options.speed or 1)
      this

    aim: (obj, options)->
      @position.copy NULL
      @speed
        .randomize()
        .multiply options.spread or NULL
        .setZ 1
        .normalize()
        .applyQuaternion obj.looking
        .multiplyScalar @speedScalar
      @lookAt @speed
      @position
        .copy options.offset or NULL
        .applyQuaternion obj.looking
        .add obj.position
      @pickTarget() if @homing
      this

    home: (game)->
      tmpVec
        .copy @target.position
        .add @target.center
        .sub @position
        .normalize()
      @speed
        .normalize()
        .lerp tmpVec, Math.min @homing * game.timer.delta, 1
      @quaternion
        .setFromUnitVectors NORTH, @speed
      @speed
        .setLength @speedScalar

    explode: ->
      hits = collisionSphere @targets,
        origin: @position
        radius: @radius
        recursive: true
      @weight *= 36
      @position.y -= meters 6
      for target in hits when target.onHit
        @speed
          .copy target.position
          .sub @position
        target.onHit game, this
      return

    pickTarget: ->
      position = @position
      speed = @speed
      scalar = @speedScalar
      targets = all @homingTargets
      @target = targets.length and _.min targets, (t)->
        dot =
          tmpVec
          .subVectors position, t.position
          .normalize()
          .dot speed
        distance = position.distanceTo t.position
        if dot < 0 then dot - distance else Infinity
