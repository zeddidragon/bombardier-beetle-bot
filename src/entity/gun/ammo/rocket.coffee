{CylinderGeometry, MeshBasicMaterial} = THREE
Bullet = require './bullet'

matrix = new Matrix4
matrix.makeRotationX Math.PI * 0.5

geometry = new CylinderGeometry meters(1), meters(1), meters(5)
geometry.applyMatrix matrix
material = new MeshBasicMaterial
  color: 0x868686


module.exports =
  class Rocket extends Bullet
    constructor: (world, data)->
      @mesh = new Mesh geometry, material
      super
      @splash = 1
      @radius = meters 36
