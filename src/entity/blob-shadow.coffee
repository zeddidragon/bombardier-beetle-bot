{CircleGeometry, MeshBasicMaterial} = THREE

Entity = require './entity'
collisionPoint = require '../utils/collision/point'


geometry = new CircleGeometry 0.5, 16
material = new MeshBasicMaterial
  color: 0
  opacity : 0.5
  transparent: true

module.exports =
  class BlobShadow extends Entity
    constructor: (world, radius)->
      @mesh = new Mesh geometry, material
      super @mesh, []
      @mesh.scale.multiplyScalar radius
      @mesh.rotation.x = -Math.PI * 0.5
      world.add this

    update: (game, position)->
      @position
        .copy UP
        .add position
      collision =
        collisionPoint game.city.mesh,
          origin: @position
          direction: DOWN
      @position
        .copy collision
        .y += 0.001
