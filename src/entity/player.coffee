{SphereGeometry, MeshPhongMaterial} = THREE

SphereHitbox = require '../hitbox/sphere'
Entity = require './entity'
CameraEntity = require './camera'
BlobShadow = require './blob-shadow'
Arrow = require './modules/arrow'

guns = [
  require './gun/af68'
  require './gun/af67-st'
  require './gun/kangaroo'
  require './gun/irwin-buster'
  require './gun/luft-wal'
]

boundary = meters 800
modules =
  gunControls: require './modules/gun-controls'
  jumping: require './modules/jumping'
  rotation: require './modules/rotation-controls'
  lookAlign: require './modules/look-align'
  movement: require './modules/movement-controls'
  physics: require './modules/verlet-physics'
  frictionPhysics: require './modules/verlet-friction'
  surfaceAgnostic: require './modules/surface-agnostic'
  gunSwitching: require './modules/gun-switching'


module.exports =
  class Player extends Entity
    constructor: (world, data)->
      @container = new Object3D
      @container.name = 'PlayerContainer'
      material = new MeshPhongMaterial
        color: 0xFF0000
      @avatar = new Mesh data.models.spiderTank, material
      @avatar.name = 'Player'
      @container.add @avatar
      @height = meters 3
      @hp = 144
      @hitbox = new SphereHitbox @height
      @guns = _.map guns, (Gun)->
        new Gun world, data, data.targets.player, data.targets.playerHoming
      super @container, modules
      @blobShadow = new BlobShadow world, @height * 2
      @camera = new CameraEntity world.camera, this

    update: (game)->
      @gunControls game
      @mesh.visible = not @isAiming
      @jumping game
      @rotation game
      @lookAlign game
      @movement game
      @physics game
      @frictionPhysics game
      @surfaceAgnostic game
      @position.clampScalar -boundary, boundary
      @position.y = Math.max 0, @position.y
      @blobShadow.update game, @position
      @gunSwitching game
      @camera.update game
