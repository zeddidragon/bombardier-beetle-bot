blit = (arr, x, y, w, h, v)->
  _.times h, (j)->
    _.times w, (i)->
      arr[y + j][x + i] = v

B_RES = 4

module.exports = (city)->
  ret = []
  ret.width = city.width
  ret.height = city.height
  ret.blockSize = city.constructor.BLOCK_SIZE
  ret.blockResolution = B_RES
  _.times city.plan.length * B_RES, (j)->
    ret[j] = new Array city.width * B_RES
  _.each city.plan, (row, j)->
    _.each row, (tile, i)->
      tile or= -1
      blit ret, i * B_RES + 1, j * B_RES + 1, 2, 2, tile
      if row[i + 1] is tile
        blit ret, i * B_RES + 3, j * B_RES + 1, 2, 2, tile
      return unless city.plan[j + 1]
      if city.plan[j + 1][i] is tile
        blit ret, i * B_RES + 1, j * B_RES + 3, 2, 2, tile
      if city.plan[j + 1][i + 1] is tile
        blit ret, i * B_RES + 3, j * B_RES + 3, 2, 2, tile
      if city.plan[j + 1][i - 1] is tile
        blit ret, i * B_RES - 3, j * B_RES + 3, 2, 2, tile
  ret
