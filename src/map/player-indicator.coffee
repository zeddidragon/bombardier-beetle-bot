{ArrowHelper} = THREE
Indicator = require './indicator'

dir = new Vector3
rotate90 = new Euler Math.PI * 0.5, 0, 0


module.exports =
  class PlayerIndicator extends Indicator
    constructor: (@obj, opts)->
      @color = 0x00FF00
      @size = 1
      @z = .2
      @worldWidth = 1
      @worldHeight = 1
      _.extend this, opts
      @mesh = new ArrowHelper UP, NULL
      @mesh.setColor @color
      @mesh.setLength 0.5, 1, 1
      {@position, @scale} = @mesh
      @position.z = -@z
      @scale.multiplyScalar @size * 0.05

    update: (game)->
      super
      dir
        .copy NORTH
        .applyQuaternion @obj.facing
        .applyEuler rotate90
      @mesh.setDirection dir
