{Scene, PlaneGeometry, MeshBasicMaterial, OrthographicCamera} = THREE
texture = require './minimap-texture'


module.exports =
  class Minimap
    constructor: (@aiMap)->
      @geometry = new PlaneGeometry 1, 1
      @material = new MeshBasicMaterial
        map: texture @aiMap
        transparent: true
        opacity: 0.75
      @mesh = new Mesh @geometry, @material
      {@position, @quaternion, @scale} = @mesh
      @scene = new Scene
      @scene.add @mesh
      @indicators = []
      w = innerWidth * 0.5
      h = innerHeight * 0.5
      @camera = new OrthographicCamera -w, w, -h, h, 1, 1000
      @camera.lookAt new Vector3(0, 0, 2)

    add: (obj)->
      @mesh.add obj.mesh
      @indicators.push obj
      obj.aiMap = @aiMap

    remove: (obj)->
      @mesh.remove obj.mesh
      _.remove @indicators, obj

    update: (game)->
      _.each @indicators, (indicator)->
        indicator.update game

    resize: (width, height)->
      w = width * 0.5
      h = height * 0.5
      @camera.left = -w
      @camera.right = w
      @camera.top = -h
      @camera.bottom = h
      scale = h * .5
      @scale.set scale, scale, 1
      scale *= .55
      @position.set scale - w, scale - h, 2
      @camera.updateProjectionMatrix()
