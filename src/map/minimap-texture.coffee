{Texture, WebGLRenderer, NearestFilter} = THREE

renderer = new WebGLRenderer
MAX_ANISOTROPY = renderer.getMaxAnisotropy()


module.exports = (aiMap)->
  canvas = document.createElement 'canvas'
  canvas.width = 96
  canvas.height = 96

  context = canvas.getContext '2d'
  context.fillStyle = '#000000'
  context.fillRect 0, 0, canvas.width, canvas.height

  imageData = context.getImageData 0, 0, canvas.width, canvas.height
  data = imageData.data

  for j in [0...canvas.height]
    for i in [0...canvas.height]
      continue unless val = aiMap[j]?[i]
      pos = i * 4 + j * canvas.width * 4
      data[pos] = (val) * 0.5
      data[pos + 1] = 100
      data[pos + 2] = 128 - val * 0.5

  context.putImageData imageData, 0, 0

  texture = new Texture canvas
  texture.minFilter = NearestFilter
  texture.anisotropy = MAX_ANISOTROPY
  texture.needsUpdate = true
  texture
