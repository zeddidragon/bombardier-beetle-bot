{PlaneGeometry, MeshBasicMaterial, ArrowHelper} = THREE

geometry = new PlaneGeometry 1, 1
material = new MeshBasicMaterial color: 0xFF0000

module.exports =
  class Indicator
    constructor: (@obj, opts)->
      @size = 1
      @z = .1
      @worldWidth = 1
      @worldHeight = 1
      _.extend this, opts
      @mesh = new Mesh geometry, material
      {@position, @scale} = @mesh
      @position.z = -@z
      @scale.multiplyScalar @size * 0.02

    update: (game)->
      @position.x = @obj.position.x / @worldWidth
      @position.y = -@obj.position.z / @worldHeight

    Object.defineProperty @prototype, 'aiMap',
      set: (map)->
        @_aiMap = map
        @worldWidth = map.width * map.blockSize / map.blockResolution * 4
        @worldHeight = map.width * map.blockSize / map.blockResolution * 4
      get: ->
        @_aiMap
