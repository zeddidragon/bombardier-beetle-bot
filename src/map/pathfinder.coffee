aStarWorkers = require '../ai/a-star-workers'

module.exports =
  class Pathfinder
    constructor: (@aiMap)->
      {@width, @height, @blockSize, @blockResolution} = @aiMap
      @width *= @blockResolution
      @height *= @blockResolution
      @worldRatio = @blockSize / @blockResolution

    toCityCoords: (vec)->
      vec.set(
        (vec.x - @width * 0.5 + 0.5) * @worldRatio, 0,
        (vec.y - @height * 0.5 + 0.5) * @worldRatio)

    toMapCoords: (vec)->
      vec.set(
        Math.floor(vec.x / @worldRatio + @width * 0.5),
        Math.floor(vec.z / @worldRatio + @height * 0.5),
        0)

    checkBounds: (x, y)->
      0 <= x < @width and 0 <= y < @height

    equals: (x, y, val)->
      @checkBounds(x, y) and val is @aiMap[y][x]

    nearest: (vec, val)->
      for distance in [0...Math.max @width, @height] by 1
        for i in [0...distance] by 1
          x = vec.x - distance + i
          y = vec.y + i
          return vec.set x, y, 0 if @equals x, y, val
          x = vec.x + i
          y = vec.y + distance - i
          return vec.set x, y, 0 if @equals x, y, val
          x = vec.x + distance - i
          y = vec.y - i
          return vec.set x, y, 0 if @equals x, y, val
          x = vec.x - i
          y = vec.y - distance + i
          return vec.set x, y, 0 if @equals x, y, val

    cutToNearest: (vec, path)->
      return path unless path.length
      nearest = path[0]
      nearestIndex = 0
      nearestDistance = Math.abs(vec.x - nearest[0]) + Math.abs(vec.y - nearest[1])
      for i in [1...path.length] by 1
        node = path[i]
        distance = Math.abs(vec.x - node[0]) + Math.abs(vec.y - node[1])
        if distance < nearestDistance
          nearest = node
          nearestIndex = i
          nearestDistance = distance
        break if nearestDistance is 0
      path.slice 0, nearestIndex

    path: (start, dest, instance, opts)->
      aStarWorkers.path [start.x, start.y], [dest.x, dest.y], instance, opts

    next: (path, vec)->
      node = path.pop()
      node and @toCityCoords vec.set node[0], node[1], 0

    random: (vec, val)->
      x = _.random @width
      y = _.random @height
      @toCityCoords @nearest vec.set x, y, 0

