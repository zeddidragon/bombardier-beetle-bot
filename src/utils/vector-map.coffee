tmpVec = new Vector3
tmpArr = []


module.exports =
  class VectorMap
    constructor: ->
      @map = {}

    key: (vec)->
      vec
        .toArray tmpArr
        .join ','

    set: (vec, value=vec)->
      @map[@key vec] = value

    get: (vec)->
      @map[@key vec]

    check: (vec, offset)->
      @get tmpVec.addVectors vec, offset
