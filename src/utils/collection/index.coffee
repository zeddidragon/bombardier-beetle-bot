module.exports =
  class Collection
    constructor: ->
      @instances = []
      @count = 0

    new: (createFunc)->
      @instances[@count++] or= createFunc()

    # Return false in func to remove the element
    each: (func, removeFunc)->
      for i in [@count-1..0] by -1
        if false is func @instances[i]
          @count--
          current = @instances[i]
          @instances[i] = @instances[@count]
          @instances[@count] = current
          removeFunc current, i if removeFunc
      return
