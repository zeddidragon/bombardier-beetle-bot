{Object3D} = THREE
Collection = require './index'

create = ->
  new @class @geometry, @material

remove = (instance)->
  @mesh.remove instance


module.exports =
  class MeshCollection extends Collection
    constructor: (@geometry, @material, @class=Mesh)->
      @mesh = new Object3D
      @create = create.bind this
      @remove = remove.bind this
      @name = 'MeshCollection'
      @instanceName = 'MeshInCollection'
      super

    new: ->
      ret = super @create
      ret.name = "#{@instanceName}##{@count}"
      @mesh.add ret
      ret

    each: (func)->
      super func, @remove
