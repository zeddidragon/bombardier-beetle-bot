{Object3D} = THREE
Collection = require './index'

create = ->
  new @class @world, @data

remove = (instance)->
  @mesh.remove instance.mesh


module.exports =
  class EntityCollection extends Collection
    constructor: (@class, @world, @data)->
      @mesh = new Object3D
      @create = create.bind this
      @remove = remove.bind this
      @name = 'EntityCollection'
      @instanceName = 'EntityInCollection'
      super

    new: ->
      ret = super @create
      ret.name = "#{@instanceName}##{@count}"
      @mesh.add ret.mesh
      ret

    each: (func)->
      super func, @remove

    update: (game)->
      @each (entity)->
        entity.update game
