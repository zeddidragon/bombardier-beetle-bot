module.exports = collect =  (target, ret)->
  ret or= []
  target.collect ret
  collect child, ret for child in target.children
  ret
