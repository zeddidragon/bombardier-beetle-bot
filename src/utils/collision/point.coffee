{Raycaster} = THREE

caster = new Raycaster
ray = caster.ray


module.exports = (target, opts)->
  caster.far = opts.distance or km 3
  ray.origin.copy opts.origin or NULL
  ray.direction
    .copy opts.direction or NORTH
    .normalize()
  hits = caster.intersectObject target, true
  if hits[0]
    hits[0].point
  else if not opts.hitsOnly
    ray.direction
      .multiplyScalar caster.far
      .add ray.origin
