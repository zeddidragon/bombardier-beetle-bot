{Raycaster} = THREE

caster = new Raycaster
ray = caster.ray


module.exports = (target, opts)->
  caster.far = opts.distance or km 3
  ray.origin.copy opts.origin or NULL
  ray.direction
    .copy opts.direction or NORTH
    .normalize()
  
  caster.intersectObject target, true
