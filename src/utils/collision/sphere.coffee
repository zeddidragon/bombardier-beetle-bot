{Sphere} = THREE

sphere = new Sphere


module.exports = spherecast = (target, opts, hits=[])->
  sphere.center.copy opts.origin or NULL
  sphere.radius = opts.radius or 1

  target.spherecast sphere, hits
  spherecast child, opts, hits for child in target.children if opts.recursive

  hits

