{Color} = THREE


module.exports =
  random: (min=0, max=1)->
    ret = new Color
    ret.r = _.random min, max, true
    ret.g = _.random min, max, true
    ret.b = _.random min, max, true
    ret
