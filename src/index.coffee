require './globals'
require './mixins'
preload = require './preload'
create = require './create'
init = require './init'
update = require './update'


data = preload
  images: ['skybox-mountain.png']
  models: ['spider-tank.obj', 'armored-car.obj', 'collision/armored-car.obj']

loaded = new Promise (cb)-> window.onload = cb

Promise
  .all [data, loaded]
  .spread (data)->
    game = create data
    window.game = game
    init game
    update game
