tmpVec = new Vector3


module.exports =
  _.mixin Matrix4.prototype,
    rotateBetween: (vec1, vec2)->
      @identity()
      return this if vec1.equals vec2
      @setX vec1
      @setZ tmpVec.crossVectors vec1, vec2
      @setY tmpVec.crossVectors tmpVec, vec1

    setX: (vec)->
      @elements[0] = vec.x
      @elements[1] = vec.y
      @elements[2] = vec.z
      this

    setY: (vec)->
      @elements[4] = vec.x
      @elements[5] = vec.y
      @elements[6] = vec.z
      this

    setZ: (vec)->
      @elements[8] = vec.x
      @elements[9] = vec.y
      @elements[10] = vec.z
      this
