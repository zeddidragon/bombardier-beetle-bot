{Sphere} = THREE
tmpSphere = new Sphere


module.exports =
  _.mixin Mesh.prototype,
    spherecast: (sphere, intersects)->
      @geometry.computeBoundingSphere() unless @geometry.boundingSphere
      tmpSphere
        .copy @geometry.boundingSphere
        .applyMatrix4 @matrixWorld
      if tmpSphere.intersectsSphere sphere
        intersects.push this

    collect: (arr)->
      arr.push this

