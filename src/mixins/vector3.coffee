tmp = new Vector3
tmpArr = []

bezier = (v1, v2, v3, t)->
  (1 - t) * (1 - t) * v1  +  2 * t * (1 - t) * v2  +  t * t * v3

module.exports =
  _.mixin Vector3.prototype,
    addScaled: (vec, scalar)->
      tmp
        .copy vec
        .multiplyScalar scalar
      @add tmp

    addInOrder: (vec, order)->
      vec.toArray tmpArr
      for i in [0..2]
        @[order[i]] += tmpArr[i]
      this

    log: (label)->
      puts "#{label} (#{@x}, #{@y}, #{@z})"
      this

    counter: (force)->
      tmp
        .copy this
        .setLength -force
      @add tmp

    tresholdScalar: (min)->
      @multiplyScalar 0 if @lengthSq() < Math.pow min, 2
      this

    maxScalar: (max)->
      @setLength max if @lengthSq() > Math.pow max, 2
      this

    randomize: ->
      @set Math.random(), Math.random(), Math.random()
      @addScalar -0.5

    bezier3: (tween, dest, t)->
      @x = bezier @x, tween.x, dest.x, t
      @y = bezier @y, tween.y, dest.y, t
      @z = bezier @z, tween.z, dest.z, t
      this

    bezier3Vectors: (start, tween, dest, t)->
      @copy start
        .bezier tween, dest, t
