lazypipe = require 'lazypipe'
maps = require 'gulp-sourcemaps'
plumber = require 'gulp-plumber'
cache = require 'gulp-cached'
remember = require 'gulp-remember'
connect = require 'gulp-connect'

config = require './config'

module.exports =
  init: (name)->
    lazypipe()
      .pipe cache, name
      .pipe plumber
      .call this

  finish: (name)->
    lazypipe()
      .pipe plumber.stop
      .pipe remember, name
      .pipe gulp.dest, config.build
      .pipe connect.reload
      .call this
