browserify = require 'browserify'
watchify = require 'watchify'
gutil = require 'gulp-util'
plumber = require 'gulp-plumber'
source = require 'vinyl-source-stream'
buffer = require 'vinyl-buffer'
connect = require 'gulp-connect'
coffeeify = require 'coffeeify'
jadeify = require 'jadeify'

config = require './config'


bundler = browserify config.browserify
watcher = watchify bundler

module.exports = bundle = ->
  watcher.bundle()
    .on 'error', (err)->
      gutil.log err
      @emit 'end'
    .pipe source config.browserify.script
    .pipe buffer()
    .pipe gulp.dest config.browserify.root
    .pipe connect.reload()

bundle()
watcher.on 'update', bundle
watcher.on 'log', (log)->
  gutil.log log
