module.exports =
  paths:
    scripts: './src/**/*.coffee'
    styles: './src/**/*.styl'
    templates: './src/index.jade'
    assets: ['./assets/**/*.{png,obj}', './src/**/*.jade']
  build: './build/'
  browserify:
    cache: {}
    packageCache: {}
    debug: true
    entries: ['./src/index.coffee']
    script: 'build.js'
    root: './build'
    transform: [
      'coffeeify'
      'jadeify'
      './transforms/blobify'
    ]
    extensions: [
      '.coffee'
      '.jade'
      '.blob.coffee'
    ]
    paths: [
      './node_modules',
      './src',
      './src/utils'
      './src/entity'
    ]
