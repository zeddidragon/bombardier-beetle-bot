{AuthDriver, Client} = require 'dropbox'
Promise = require 'bluebird'
through = require 'through2'

Promise.promisifyAll Client.prototype

module.exports = (opts)->
  client = new Client opts
  auth = client.authenticateAsync()

  through.obj (file, enc, cb)->
    auth.then ->
      client.writeFile file.relative, file.contents, cb
