config = require '../config'


task 'assets', ->
  gulp.src config.paths.assets
    .pipe gulp.dest config.build
