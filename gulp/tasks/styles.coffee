styl = require 'gulp-stylus'
maps = require 'gulp-sourcemaps'

boilerplate = require '../boilerplate'
config = require '../config'

task 'styles', ->
  gulp.src config.paths.styles
    .pipe boilerplate.init 'styles'
    .pipe maps.init()
    .pipe styl()
    .pipe maps.write()
    .pipe boilerplate.finish 'styles'
