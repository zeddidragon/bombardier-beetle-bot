remember = require 'gulp-remember'
cache = require 'gulp-cached'

config = require '../config'


watch = (assetType)->
  gulp.watch config.paths[assetType], [assetType]
    .on 'change', (event)->
      if event.type is 'deleted'
        delete cache.caches[assetType]?[event.path]
        remember.forget assetType, event.path

task 'watch', ['build'], ->
  watch 'scripts'
  watch 'styles'
  watch 'templates'
  watch 'assets'
