connect = require 'gulp-connect'
gutil = require 'gulp-util'


task 'serve', ['watch'], ->
  connect.server
    root: ['build', 'assets']
    livereload: true
