jade = require 'gulp-jade'

boilerplate = require '../boilerplate'
config = require '../config'

task 'templates', ->
  gulp.src config.paths.templates
    .pipe boilerplate.init 'templates'
    .pipe jade()
    .pipe boilerplate.finish 'templates'
