var through = require('through2')
var browserify = require('browserify')

var config = { transform: [ 'coffeeify' ] }

module.exports = function (file, options){
  if (file.indexOf('.blob') < 0) return through()

  function throughput (data, enc, cb) {
    code = [
      'module.exports=',
        'new Blob(',
          '[' + JSON.stringify(data.toString('utf8')) + ']',
          ',{type: "text/javascript"}',
        ');']
    this.push(code.join(''))
    cb()
  }

  return browserify(file, config)
    .bundle()
    .pipe(through(throughput))
}
